﻿namespace SigAuth

open System.Text.Encodings.Web
open Microsoft.AspNetCore.Authentication
open Microsoft.Extensions.Options
open Microsoft.Extensions.Logging
open FSharp.Control.Tasks.V2.ContextInsensitive


type SignatureAuthenticationOptions() =
    inherit AuthenticationSchemeOptions()

type SignatureAuthenticationHandler
    (optionsMonitor:IOptionsMonitor<SignatureAuthenticationOptions>,
     loggerFactory:ILoggerFactory, 
     encoder:UrlEncoder, 
     clock:ISystemClock) =
    inherit AuthenticationHandler<SignatureAuthenticationOptions>
        (optionsMonitor, loggerFactory, encoder, clock)

    override this.HandleAuthenticateAsync() = task {
        return AuthenticateResult.NoResult()
    }